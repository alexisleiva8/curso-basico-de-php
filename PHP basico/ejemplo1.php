<html>
    <head>
        <title>
            Ejemplo 1 || Hola mundo
        </title>
    </head>
    <body>
        <!--Dentro de este bloque se usa la sintaxis de php-->
        <?php 
        
        //"echo" es la palabra reservada que usa php para mostar algo en pantalla
        echo "Hola mundo"; //las comillas dobles hacen referncia a una cadena de texto o lo que se conoce como String
                            //tammbien podemos usar comillas simples para mostrar texto

        ?>
        <!--Se cierra el bloque de codigo php-->
    </body>
</html>