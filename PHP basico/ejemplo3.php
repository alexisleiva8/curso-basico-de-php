<html>
    <head>
        <title>
            Ejemplo 3 || Estructuras de control
        </title>
    </head>
    <body>
    <center>
        <?php             
            /*
            Uso de las esstructuras de control
            */

            //Declaracion de variables
            $universidad = 'Universidad Tecnologica De El Salvador';
            $nombre = 'Jonathan';
            $apelllido = 'Leiva';
            $carrera = 'Ingenieria en sistemas';
            echo "ESTRUCTURA IF/ELSE<br><br>";
            //Estructura if, Se hace  una comparacion(==) del valor de la variable nombre, si esta se cummple se ejecutara la sentencia echo
            if($nombre == "Jonathan"){
                echo $nombre," ",$apelllido," es estudiante de la ",$universidad," en la carrera: ",$carrera,"<br><br>";
            }else//este bloque entraria en funcion si al realizar la comparacion esta no se cumpliera
            {
                echo "Estudiante no registrado";
            }

            //Estructura for
            echo "<br><br>ESTRUCTURA FOR<br><br>";
            //La variable $i(Altura) inicia en valor de 1 y  se repetira asta que sea menor o igual a 10(filas)
            for($i=1; $i<=10; $i++){
                //asteriscos
                 //La variable $x inicia en valor de 1, este ciclo imprimira astericos asta que x sea menor o igual que i
                for($x=1; $x<=$i;$x++){
                    echo "*";
                }
                    echo "<br>";//saltos de linea
            }

            //Estructura for
            echo "<br><br>ESTRUCTURA WHILE<br><br>";
            $i = 1;
            /*
            El significado de una sentencia while es simple. 
            Le dice a PHP que ejecute las sentencias anidadas, tanto como la expresión while se evalúe como TRUE.
            */
            while ($i <= 10) {
                echo $i++,", ";  /* el valor presentado sería
                   $i antes del incremento
                   (post-incremento) */
            }

            //Estructura for
            echo "<br><br>ESTRUCTURA DO-WHILE<br><br>";
            $i=0;
            /*
            Los bucles do-while son muy similares a los bucles while, 
            excepto que la expresión verdadera es verificada al final de cada iteración en lugar que al principio.
            */
            do {
                echo $i;
            } while ($i > 0);
            /*
            El bucle de arriba se ejecutaría exactamente una sola vez, ya que después de la primera iteración, 
            cuando la expresión verdadera es verificada, se evalúa como FALSE ($i no es mayor que 0) y termina 
            la ejecución del bucle.
            */


        ?>
    </center>
    </body>
</html>