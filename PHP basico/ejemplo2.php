<html utf8_decode>
    <head>
        <title>
            Ejemplo 2 || Uso de variables
        </title>
    </head>
    <body>
        <!--Dentro de este bloque se usa la sintaxis de php-->
        <?php 
        /* 
        Para utilizar las variables php, no es necesario definir el tipo de dato que almacenara;
        ya que php reconoce el valor que esta toma. Las variables tipo string se pueden inicializar o dejar 
        vacias y se tomara un valor de cadena vacia
        */
        $cadena ="";

        /* 
        Las variables nnumericas por buena practica se inicializan en valor 0.
        Tambien peden quedar como: $numero = ; y esta tomara el valor de 0
        */
        $numero = 0;

        /* 
        Cuando se usa una variable booleana(true o false) se puede dejar vacia y esta tomara el valor de
        false por defecto
        */
        $logico = true;

        /* 
        Ejemplo de uso basico de las variables
        1- operaciones aritmeticas
        2- cadenas string
        3- uso de booleanos
        */

        /*
        1- operaciones artimeticas basicas 
        2- uso de cadenas estring
        */
        $num1 = 750;
        $num2 = 10;

        echo "1- operaciones artimeticas basicas <br>";
        //para hacer un salto de linea inscrustamos etiquetas html dentro de php de la siguiente manera:
        echo "<br><br>";
        //suma
        $suma = $num1 + $num2;
        echo "La suma de 750 y 10 es: ", $suma;

        echo "<br><br>";
        //multiplicacion
        $multiplicacion = $num1 * $num2;
        echo "La multiplicacion de 750 y 10 es: ", $multiplicacion;

        echo "<br><br>";
        //resta
        $resta = $num1 - $num2;
        echo "La resta de 750 y 10 es: ", $resta;

        echo "<br><br>";
        //division
        $divison = $num1 / $num2;
        echo "La divison de 750 y 10 es: ", $divison;
        echo "<br><br>";
        //3- uso de valores booleanos
        echo "3- uso de las variables de booleanas <br><br>";
        $bool1= true;

        //2- uso de cadenas estring
        echo "2- uso de las variables de cadena <br><br>";
        $nombres="Rebeca Saraí";
        $apellidos = "Orellana Miranda";
        
        //se hace uno de la concatenacion de variables. El resultado sera Rebeca Saraí Orellana Miranda
        echo $nombres," ",$apellidos,"<br><br>";
    
        //uso basico de la estructura if
        if($bool1 != false){
            echo "El resultado al usar una comparacion boolenada de la variable Sbool1 = true mediante
            difernete que (!= false), es TRUE";

        }
        else{//Este mensaje se mostraria si la variable bool1 tuvuera como valor false y al realizar la comparacion regresaria un valor false
            echo "El resultado al usar una comparacion boolenada \n de la variable Sbool1 = true mediante
            \n difernete que (!= false), es FALSE";
        }



        ?>
        <!--Se cierra el bloque de codigo php-->
    </body>
</html>